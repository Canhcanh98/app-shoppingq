import { getAll } from '../../apis/Product'

import { VuexModule, Module, Action } from 'vuex-module-decorators';

@Module({ namespaced: false })
class Product extends VuexModule {
    public productList: any[] = [];

    @Action
    public getProduct(): void {
        getAll("nike/men/shorts")
            .then((response) => {
                this.context.state.productList = response;
            })
    }
}
export default Product;