import axios from "axios";
import { BASE_URL } from "../constants/configApi";

const callApi = axios.create({
  baseURL: BASE_URL,
});

export default callApi;
