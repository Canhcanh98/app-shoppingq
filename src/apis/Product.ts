import callApi from './axiosClient'

export const getAll = (endpoint: string) => {
  return callApi({
    url: endpoint,
    method: 'GET',
  })
};